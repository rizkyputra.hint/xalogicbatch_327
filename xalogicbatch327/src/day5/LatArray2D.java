package day5;

import java.util.Scanner;

public class LatArray2D {

	public static void main(String[] args) {
		Array2Dim();
	}
	
	static void Array2Dim_2() {
		Scanner input=new Scanner(System.in);
		
		String[][] meja = new String[2][3];
		
		for(int i = 0; i< meja.length; i++) {
			for(int j = 0; j < meja[i].length; j++) {
				System.out.format("Siapa yang duduk di meja (%d,%d): ", i,j);
				meja[i][j] = input.nextLine();
			}
		}
		for(int i=0; i < meja.length; i++) {
			for(int j = 0; j < meja[i].length; j++) {
				System.out.format("|%s|\t", meja[i][j]);
			}
			System.out.println(); 
		} input.close();
	}
	
	
	static void Array2Dim() {
		String[][] login = 
		{
				{"Administrator", "Admin"},
				{"StaffIT", "StafIT"},
				{"Staff Finance", "StafFinance"}
		};
		
		for(int i = 0; i < login.length; i++) {
			for( int j = 0; j < login[i].length; j++) {
				System.out.println("Username : " + login[i][j] + "\t");
//				System.out.println("Password : " + login[i][j]);
			}
			System.out.println();
		}
		int[][] arrInt = {{ 1,2,3},{4,5,6},{7,8,9}};
		for (int i = 0; i<arrInt.length; i++) {
			for (int j = 0; j<arrInt[i].length; j++) {
				System.out.print(arrInt[i][j] + "\t");
			}
			System.out.println();
		}
	}

}
