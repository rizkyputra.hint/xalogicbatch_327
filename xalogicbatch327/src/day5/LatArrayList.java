package day5;
import java.util.ArrayList;
public class LatArrayList {

	public static void main(String[] args) {
		ArrayList();

	}
	static void ArrayList() {
		ArrayList aris = new ArrayList();
		
		aris.add("Leo");			//0
		aris.add(100);				//1
		aris.add(123.456);			//2
		aris.add(true);				//3
		aris.add("Rizal");			//4
		
		System.out.println(aris);
		
		System.out.println(aris.get(0));
		System.out.println(aris.get(3));
		System.out.println(aris.get(4));
		
		aris.remove(1);
		aris.remove(true);
		
		System.out.println(aris);
		
		for(int i=0; i < aris.size(); i++) {
			System.out.println(aris.get(i));
		}
		
		for(Object i : aris) {					 //For Each
			System.out.println(i);
		}
	}
}
