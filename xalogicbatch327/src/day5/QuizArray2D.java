package day5;

import java.util.Scanner;

public class QuizArray2D {

	public static void main(String[] args) {
		soal1();
	}
	
	static void soal1() {
		
		Scanner input=new Scanner(System.in);
		
		System.out.print("Masukan Jumlah Kata : ");
        int n1=input.nextInt();
        System.out.print("Masukan kelipatannya : ");
        int n2=input.nextInt();
	
        int[][] deret = new int[n2][n1];
        
        for (int i=0; i<deret.length; i++) {				// For buat list
        	System.out.println();
        	for(int j = 0; j < deret[i].length; j++) {		// For buat kolom
        		if(i == 0) {
        			deret[i][j] = 0 + j;
        			System.out.print(deret[i][j] + " ");
        		}
        		else if ( i==1) {
        			if(j > 0) {
        				deret[i][j] = deret[i][j-1]*n2;
        			}else {
        				deret[i][j] = 1;
        			}
        			System.out.print(deret[i][j] + " ");
        		}else {
        			deret[i][j]= 0;
        		}
        	}
        	input.close();
        }
	}
	
	static void soal2() {
		Scanner input=new Scanner(System.in);
		
		System.out.print("Masukan Jumlah Kata : ");
        int n1=input.nextInt();
        System.out.print("Masukan kelipatannya : ");
        int n2=input.nextInt();
	
        int[][] deret = new int[n2][n1];
        
        for (int i=0; i<deret.length; i++) {					// For buat list
        	System.out.println();
        	for(int j = 0; j < deret[i].length; j++) {			// For buat kolom
        		if ( i % 3 == 0 && i!= 0 ) {
    				System.out.print("-" + deret[i][j] + "");
    	    	} 
        		if(i == 0) {
        			deret[i][j] = 0 + j;
        			System.out.print(deret[i][j] + " ");
        		}
        		else if ( i==1) {
        			if(j > 0) {
        				deret[i][j] = deret[i][j-1]*n2;
        			}else {
        				deret[i][j] = 1;
        			}
        			System.out.print(deret[i][j] + " ");
        		}else {
        			deret[i][j]= 0;
        		}
        	}
        	input.close();
        }
	}
	
	
	static void soal5() {
		
		Scanner input=new Scanner(System.in);
		System.out.print("N : ");
		int n = input.nextInt();
		int mulai = 1;
		int arr [][] = new int [3][n];
		
		for(int i=0; i<arr.length; i++ ) {
			for(int j=0; j<arr[i].length; j++) {
				if(i==0) {
					arr[i][j]=j;
				}else if(i==1) {
					arr[i][j]=mulai;
					mulai=mulai*n;
				}else {
					arr[i][j]=arr[i-1][j]+j;
				}
			}
		}
		for(int i=0; i<arr.length; i++) {
			for(int j=0; j<arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
		input.close();
			
		}
	
	static void soal6() {
		Scanner input=new Scanner(System.in);
		System.out.print("N : ");
		int n = input.nextInt();
		int mulai = 1;
		int arr [][] = new int [3][n];
		
		for(int i=0; i<arr.length; i++ ) {
			for(int j=0; j<arr[i].length; j++) {
				if(i == 0) {
					arr[i][j] = j;
				}else if(i == 1) {
					arr[i][j] = mulai;
					mulai = mulai*n;
				}else {
					arr[i][j]=arr[i-1][j] + j;
				}
			}
		}
		for(int i=0; i<arr.length; i++) {
			for(int j=0; j<arr[i].length; j++) {
				System.out.print(arr[i][j] + "\t");
			}
			System.out.println();
		}
		input.close();
			
		}
	
	static void soal8() {
	Scanner input=new Scanner(System.in);
	System.out.print("N : ");
	int n = input.nextInt();
	int ganjil = 0, genap = 0;
	int arr [][] = new int [3][n];
	
	for(int i=0; i<arr.length; i++ ) {
		for(int j=0; j<arr[i].length; j++) {
			if(i==0) {
				arr[i][j]=j;
			}else if(i==1) {
				arr[i][j]=genap;
				genap=genap+2;
			}else {
				arr[i][j]=ganjil;
				ganjil=ganjil+3;
			}
		}
	}
	for(int i=0; i<arr.length; i++) {
		for(int j=0; j<arr[i].length; j++) {
			System.out.print(arr[i][j]+"\t");
		}
		System.out.println();
	}
	input.close();
		
	}
	static void soal9() {
		Scanner input=new Scanner(System.in);
		System.out.print("N : ");
		int n = input.nextInt();
		int ganjil = 0, genap = 0;
		int arr [][] = new int [3][n];
		
		for(int i=0; i<arr.length; i++ ) {
			for(int j=0; j<arr[i].length; j++) {
				if(i==0) {
					arr[i][j]=j;
				}else if(i==1) {
					arr[i][j]=genap;
					genap=genap+3;
				}else {
					arr[i][j]=ganjil;
					ganjil=ganjil+4;
				}
			}
		}
		for(int i=0; i<arr.length; i++) {
			for(int j=0; j<arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
			
		}
	static void soal10() {
		Scanner input=new Scanner(System.in);
		System.out.print("N : ");
		int n = input.nextInt();
		System.out.print("Kolom : ");
		int n1 = input.nextInt();
		int ganjil = 0, genap = 0;
		int arr [][] = new int [n1][n];
		
		for(int i=0; i<arr.length; i++ ) {
			for(int j=0; j<arr[i].length; j++) {
				if(i==0) {
					arr[i][j]=j;
				}else if(i==1) {
					arr[i][j]=genap;
					genap=genap+3;
				}else {
					arr[i][j]=ganjil;
					ganjil=ganjil+4;
				}
			}
		}
		for(int i=0; i<arr.length; i++) {
			for(int j=0; j<arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t");
			}
			System.out.println();
		}
			
		}
}
