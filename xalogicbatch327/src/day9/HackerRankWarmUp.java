package day9;

import java.util.ArrayList;
import java.util.Scanner;

public class HackerRankWarmUp {

	public static void main(String[] args) {
		staircase();

	}

	static void staircase() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan Jumlah Kata : ");
		int n = input.nextInt();
		int counter = 0;
		for (int i = 0; i < n; i++) {
			counter++;
			for (int k = n - 2; k >= i; k--) {
				System.out.print("-");
			}
			for (int j = 0; j <= counter - 1; j++) {
				System.out.print("#");
			}

			System.out.print("\n");

			input.close();
		}
	}

	static void minMax() {
		Scanner in = new Scanner(System.in);
		System.out.print("Masukan Angka : ");
		int n = in.nextInt();
		long arr[] = new long[n];
		for (int arr_i = 0; arr_i < n; arr_i++) {
			arr[arr_i] = in.nextLong();
		}
		long minVal = 0, maxVal = 0;
		for (int i = 0; i < n; i++) {
			long minf = sumOfNumbers(i, arr);
			long maxf = sumOfNumbers(i, arr);
			if (i == 0) {
				minVal = minf;
				maxVal = maxf;
			}
			if (minf < minVal) {
				minVal = minf;
			}
			if (maxf > maxVal) {
				maxVal = maxf;
			}
		}
		System.out.println(minVal + " " + maxVal);
		in.close();
	}

	static long sumOfNumbers(int a, long[] arr) {
		long sum = 0;
		for (int i = 0; i < 5; i++) {
			if (a != i) {
				sum += arr[i];
			}
		}
		return sum;
	}

	static void timeConversion() {
		Scanner sc = new Scanner(System.in);
		String dt = sc.next();
		char ap = dt.charAt(dt.length() - 2);
		dt = dt.substring(0, dt.length() - 2);
		if (ap == 'A') {
			int hh = Integer.parseInt(dt.substring(0, 2));
			if (hh == 12)
				hh = 0;
			String s = Integer.toString(hh);
			if (s.length() == 1) {
				s = "0" + s;
			}
			System.out.println(s + dt.substring(2, dt.length()));
		} else {
			int hh = Integer.parseInt(dt.substring(0, 2));
			if (hh != 12)
				hh += 12;
			String s = Integer.toString(hh);
			if (s.length() == 1) {
				s = "0" + s;
			}
			System.out.println(hh + dt.substring(2, dt.length()));
			sc.close();
		}
	}

	static void simpleArray() {

		Scanner input = new Scanner(System.in);

		System.out.print("Masukan Jumlah Kata : ");
		int n = input.nextInt();
		int arr1[] = new int[n];
		int sum = 2;
		for (int i = 0; i < arr1.length; i++) {
			System.out.print(sum + " ");
			sum = sum + 2;
		}
		input.close();

	}

	static void plusminus() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan panjang array : ");
		int n = input.nextInt();
		int arr[] = new int[n];
		int pos = 0, neg = 0, zero = 0;
		for (int i = 0; i < n; i++) {
			arr[i] = input.nextInt();
			if (arr[i] < 0) {
				neg++;
			} else if (arr[i] > 0) {
				pos++;
			} else if (arr[i] == 0) {
				zero++;
			}
		}
		float a = 0f, b = 0f, c = 0f;
		a = (float) pos / n;
		b = (float) neg / n;
		c = (float) zero / n;
		System.out.printf("%.6f \n", a);
		System.out.printf("%.6f \n", b);
		System.out.printf("%.6f", c);
		input.close();
	}

	static void plusminus2() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan panjang array : ");
		ArrayList aris = new ArrayList();
		int n = input.nextInt();
		int arr[] = new int[n];
		
		for(int i = 0;i<n; i++) {
			int element = input.nextInt();
			arr.add(element); 
			
		}
	}
}
