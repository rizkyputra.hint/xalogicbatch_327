package day3;

import java.util.Scanner;

public class LatihanSwitchCase {

	public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
	
		System.out.println("Latihan Switch Case");
		System.out.println("========================");
		System.out.print("Pilih nomor 1-10 : ");
		String number = input.nextLine();
		System.out.println("========================");
		
		switch(number) {
			case "1":
				System.out.println("ini nomor : " + number);
				break;
			case "2":
				System.out.println("ini nomor : " + number);
				break;
			case "3":
				System.out.println("ini nomor : " + number);
				break;
			case "4":
				System.out.println("ini nomor : " + number);
				break;
			case "5":
				System.out.println("ini nomor : " + number);
				break;
			case "6":
				System.out.println("ini nomor : " + number);
				break;
			case "7":
				System.out.println("ini nomor : " + number);
				break;
			case "8":
				System.out.println("ini nomor : " + number);
				break;
			case "9":
				System.out.println("ini nomor : " + number);
				break;
			case "10":
				System.out.println("ini nomor : " + number);
				break;
				
			default :
				System.out.print("Gada pilihannya kak =)");
		}
		input.close();
	}

}
