package day3;

public class LatForEach {

	public static void main(String[] args) {
	
	int[] array = { 1, 2, 3, 4, 5};

	for(int item : array) {
		System.out.println(item);
	}
	String[] array2 = {"Ryan","Amien", "Irfan", "Khalila", "Aflahah"};
	for(String name : array2) {
		System.out.println(name +"\t");
}
	System.out.println(); //PEMISAH
	
	//SPLIT
	String kalimat = "Saya,adalah,mahasiswa";
	String[] arrkata = kalimat.split(",");
	
	for (String kata : arrkata) {
		System.out.print(" " + kata);
	}
	System.out.println(); //PEMISAH
	
	//TO CHAR ARRAY
	String kata  = "Saya";
	char [] arrChar = kata.toCharArray();
	for(char c : arrChar){
		System.out.println(c);
	}
	for (int i=0; i<arrChar.length; i++) {
		System.out.println(arrChar[i]);
	
}
}
}