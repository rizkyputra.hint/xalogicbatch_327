package day2;

import java.util.Scanner;

public class Soal1 {

	public static void main(String[] args) {
		
		Soal1();
	}
	
	static void Soal1() {
		Scanner input = new Scanner(System.in);
		
		int  ongkir, cas = 0;
		double diskon;
		
		System.out.print("Belanja: ");
		int belanja = input.nextInt();
		
		System.out.print("Jarak : ");
		int jarak = input.nextInt();
		
		System.out.print("Masukan Kode Promo : ");
		String promo = input.next();
		
		
		if(promo.equalsIgnoreCase("JKTOVO")) {
			if(belanja >= 30000) {
				diskon = belanja * 0.4;
				}
			else {
				diskon = 0;
			}
			
		if(jarak>5) {
			ongkir = 5000;
			cas = (jarak-5)*1000;
		}
		else {
			ongkir = 5000;
		}
		
		System.out.println("Belanja 	   : "+ belanja);		
		System.out.println("Diskon  	   : "+ (int)diskon);
		System.out.println("Ongkir         : "+ (ongkir+cas));
		System.out.println("Total Belanja  : "+ (int)((belanja-diskon) + (ongkir+cas)));
		
		}	
		input.close();
	}
	
	static void Soal2() {
		Scanner input = new Scanner(System.in);
	     
	    int belanja, diskon, diskon_ongkir, ongkir; 
	    
	    System.out.println("Belanja: ");
	    belanja = input.nextInt();
	    System.out.println("Ongkir: ");
	    ongkir = input.nextInt();
	     
	   
	    if (belanja >=100000) {
	      diskon = 10000;
	      diskon_ongkir = 20000;
	    }
	    else if (belanja >=50000){
	      diskon = 10000;
	      diskon_ongkir = 10000;
	    }
	    else if (belanja >=30000) {
	      diskon = 5000;
	      diskon_ongkir = 5000;
	    } 
	   
	    else {
	      diskon = 0;
	      diskon_ongkir = 0;
	    }
	  
	    
	  System.out.println("===========================================================================");
	  System.out.println("Belanja        : Rp."+ belanja);
	  System.out.println("Ongkos Kirim   : Rp."+ ongkir );
	  System.out.println("Diskon ongkir  : Rp."+ diskon_ongkir);
	  System.out.println("Diskon belanja : Rp."+ diskon);
	  System.out.println("Total Bayar    : Rp." + ((belanja-diskon) + (ongkir - diskon_ongkir)));
	  
	  
	  
	  
	  input.close();
	}

	static void Soal3() {
		Scanner input = new Scanner(System.in);
		 
		System.out.print("Masukan Pulsa : ");
		int bpulsa = input.nextInt();
		
		int point3 = 0, point2= 0;
		
		if(bpulsa>10000) {
			System.out.println("0");
			point3=(bpulsa-30000)/1000*2;
			System.out.print(" + 20 + " + point3);
		} else if (bpulsa<=30000) {
			point2 = (bpulsa-10000)/1000;
			System.out.println(" + " + point2);
		}
		input.close();
		
	}
	static void Soal4() {
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Pulsa : ");
		int pulsa = input.nextInt();
		
		
		if(pulsa >= 100000) {
			System.out.println("Point : 800");
		}
		else if (pulsa >= 50000) {
			System.out.println("Point : 400");
		}
		else if (pulsa >= 25000) {
			System.out.println("Point : 200");
		}
		else if (pulsa >= 10000) {
			System.out.println("Point : 80");
		}
		else {
			System.out.println("Masukan Angka yang benar brader");
		}
		input.close();
		
	}
	static void Soal5() {
		 
		Scanner Jari_Jari = new Scanner(System.in);

        Integer r,d ; 
        double Luas, Keliling;

        System.out.print("Jari-Jari: ");
        r = Jari_Jari.nextInt();
        
        if( r < 0) {
        	System.out.println("jangan minus");
}
        else {
        d = 2 * r ;
        Keliling = Math.PI * d ;
        Luas = Math.PI * Math.pow(r,2);
        
       
        System.out.println("Luas: "+ Luas);
        System.out.println("Keliling: "+ Keliling);
        Jari_Jari.close();
        }
    }
    
	static void Soal6 () {
		 Scanner nilai = new Scanner (System.in);
         
         int sisi;
         System.out.print("Masukan sisi = ");
         sisi = nilai.nextInt();
         
         int Luas , Keliling ;
         if (sisi < 0) {
        	 System.out.println("Jangan minus guys");
         }
         else {
         Luas = sisi * sisi;  
         System.out.println("Luas= "+Luas);  
         
         Keliling = sisi*sisi*sisi*sisi;
         System.out.println("Keliling  = "+Keliling);
         nilai.close();
	}
         }
	
	static void Soal7 () {
		Scanner input = new Scanner(System.in);
		System.out.println("Modulus");
		System.out.println("-------------------------");
		
		System.out.print("Masukan Angkanya :");
		int angka = input.nextInt();
		System.out.print("Masukan Pembaginya : ");
		int pembagi = input.nextInt();
		
		double mod = angka % pembagi;
		
		if (mod == 0) {
			System.out.print("Angka "+ angka + " % " + pembagi + " adalah 0");
		} else {
			System.out.print("Angka "+ angka + " % " + pembagi + " adalah mod");
		}
		
		input.close();
}
}


