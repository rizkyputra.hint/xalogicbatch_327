package day2;

import java.util.Scanner;

public class Percabangan {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan nama : ");
		String nama = input.next();
		
		System.out.print("Masukan umur : ");
		int umur = input.nextInt();
	
		System.out.println("Nama anda : " + nama);
		System.out.println("Umur anda : " + umur);
		if(umur ==18) {
			System.out.println("Sudah cukup umur"); 
		} else {
			System.out.println("Umur belum cukup");
		}
			input.close();
	}

}
