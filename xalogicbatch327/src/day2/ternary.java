package day2;

public class ternary {

	public static void main(String[] args) {
		
		String title = "";
		boolean isMale = true;
		
		if(isMale)
			title = "Mr.";
		else
			title = "Ms.";
	
		title = isMale ? "Mr. " : "Ms. ";
	
		System.out.println(title);
	}

}
