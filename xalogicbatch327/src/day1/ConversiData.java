package day1;

import java.util.Scanner;

public class ConversiData {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Double luas;
		int alas , tinggi;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("== Program hitung luas segitiga ==");
		System.out.print("Masukan alas : ");
		alas = input.nextInt();
		System.out.print("Masukan tinggi : ");
		tinggi = input.nextInt();
		
		luas = (double)((alas * tinggi) /2);                  //CARA CONVERTNYA
		
		System.out.format("Luasnya adalah = %s ", luas);
		
		input.close();
	}

}
