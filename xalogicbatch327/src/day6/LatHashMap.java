package day6;
import java.util.HashMap;
public class LatHashMap {

	public static void main(String[] args) {
		
		HashMap();
	}
	static void HashMap() {
		HashMap<Integer, String> days = new HashMap<Integer, String>();
		
		//add data
		days.put(1, "Senin");
		days.put(2, "Selasa");
		days.put(3, "Rabu");
		days.put(4, "Kamis");
		days.put(5, "Jumat");
		days.put(6, "Sabtu");
		days.put(7, "Minggu");
		
		//edit data
		days.replace(7, "Boleeeh");
		
		
		System.out.println(days);
		
		//ngambil data
		System.out.println(days.get(7));
		
		//Hapus
		days.remove(1);
		System.out.println(days);
		
		//HAPUS SEMUA
		days.clear();
		System.out.print(days);
		
	}
}
