package day6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class TugasDay6 {

	public static void main(String[] args) {
		
		soal6();
	}
	
	static void soal1() {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Uang Andi : ");
		String uang=input.nextLine();
		System.out.print("Harga Baju : ");
		String baju=input.nextLine();
		System.out.print("Harga Celana : ");
		String celana=input.nextLine();
		
	}
	static void soal2() {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan uang : ");
		String text=input.nextLine();
		
		String strSplit = "Hello apa kabar ?";
		String[] strArray = strSplit.split(" ");
		System.out.println(Arrays.toString(strArray));
		System.out.println(String.join("\t", strArray));
	
	}
	
	static void soal3() {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Total puntung yang ditemukan	: ");
		int puntung = input.nextInt();
		
		
		int proses = puntung/8;
		int rokokUtuh = proses;
		int sisapuntung = puntung % 8;
		
		int prosesUang=proses*500;
		int hasilUang = prosesUang;
		
		if(puntung<0) {
			System.out.print("HARAP MASUKAN ANGKA YANG BENAR =)");
		} else if (puntung==0) {
			System.out.print("APAKAH HARI INI KOSONG? =(");
		}else {
		System.out.print("Total Rokok yang dihasilkan 	: " + rokokUtuh + " Batang");
		System.out.println();
		System.out.println("Uang yang dihasilkan 		: Rp." + hasilUang);
		if(sisapuntung!=0)
			System.out.print("Sisa puntung : " + sisapuntung );
		input.close();
	}
}
	
	static void soal5( ) {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan Kalimat : ");
		String kalimat = input.nextLine().toLowerCase();
		ArrayList<String> vokal = new ArrayList<String>();
		ArrayList<String> konsonan = new ArrayList<String>();
		char ch;
		for (int i = 0; i < kalimat.length(); i++) {
			ch = kalimat.charAt(i);
			if(ch=='a'||ch=='i'||ch=='u'||ch=='e'|| ch=='o') {
				vokal.add(String.valueOf(ch));
			}else {
				konsonan.add(String.valueOf(ch));
			}
		}
		
		Collections.sort(vokal);
		Collections.sort(konsonan);
		System.out.print("Vokal 		: ");
		for (int i = 0; i<vokal.size(); i++) {
			System.out.print(vokal.get(i));
		}
		System.out.println();
		System.out.print("Konsonan	: ");
		for (int i = 0; i<konsonan.size(); i++) {
			System.out.print(konsonan.get(i));
		}
		System.out.println();
		input.close();
		    }
	static void soal6() {
		Scanner input = new Scanner(System.in);
		
		
		System.out.print("Masukan kata : ");
		String text=input.nextLine();
		String[] strSplit = text.split(" ");
		
		for(int i=0; i<strSplit.length; i++) {
		char[] Vchar = strSplit[i].toCharArray();
			for(int j = 1; j <=Vchar.length; j++) {
				if ( j%2==0)
					System.out.print("*");
				else {
					System.out.print(Vchar[j-1]);
				}
}
			System.out.print(" ");
			input.close();
		}
	}
}
