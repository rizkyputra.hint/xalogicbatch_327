package day6;

import java.util.Arrays;

public class LatString {

	public static void main(String[] args) {
	
	
		String();
	}
	
	static void String() {
	String str = "HELLO";
	String str2 = "Hi";
	String str3 = "Hello";	
	
	//PANJANG STRING
	System.out.println("Panjang String " + str + " Adalah " + str.length());
	
	//Compare String
	boolean b1 = str.equals(str2);
	boolean b2 = str.equals(str3);
	boolean b3 = str.equalsIgnoreCase(str3);
	
	int int1 = "HI".compareTo(str2);
	int int2 = "abc".compareTo("xyz");
	int int3 = "xyz".compareTo("abc");
	
	System.out.println(b1);
	System.out.println(b2);
	System.out.println(b3);
	
	System.out.println(int1);
	System.out.println(int2);
	System.out.println(int3);
	
	char chr = str.charAt(4);
	System.out.println(chr);
	
	if(!str.isEmpty())
		System.out.println("True");
	else
		System.out.println("False");
	
	int index = str.indexOf('e');
	int index2 = str.indexOf('H');
	int index3 = str.indexOf('L');
	int index4 = str.lastIndexOf('L');
	int index5 = str.lastIndexOf("LL");
	
	System.out.println(index);
	System.out.println(index2);
	System.out.println(index3);
	System.out.println(index4);
	System.out.println(index5);
	
	//Convert valueOf
	int nilai = 1969;
	String s1 = String.valueOf(nilai);
	String s2 = String.valueOf('C');
	String s3 = "C";
	
	System.out.println(s1);
	System.out.println(s2);
	System.out.println(s3);
	
	//SUBSTRING
	
	System.out.println(str.substring(1));
	System.out.println(str.substring(1,4)); //ambil koordinat index
	
	//TRIM
	String strTrim = "     HELLO      ".trim();
	String strTrim2 = "     HELLO APA KABAR ?      ".trim();
	System.out.println(strTrim);
	System.out.println(strTrim2);
	
	//REPLACE
	String strReplace = "Hello".replace('e', 'a');
	String strReplace2 = "HELLO APA KABAR ?".replace("APA", "ADA");
	System.out.println(strReplace);
	System.out.println(strReplace2);
	
	//START SWITCH & ENDSWITCH
	String strwith = "Hello apa kabar";
	if(strwith.startsWith("Hell"))
		System.out.println("String dimulai dari Hell");
	else
		System.out.println("String tidak dimulai dari Hell");
	
	if(strwith.endsWith("kabar"))
		System.out.println("String diakhiri kabar");
	else
		System.out.println("String tidak diakhiri kabar");
	
	//SPLIT
	String strSplit = "Hello apa kabar ?";
	String[] strArray = strSplit.split(" ");
	System.out.println(Arrays.toString(strArray));
	System.out.println(String.join("\t", strArray));
	
	//toCharArray()
	String strToCharArray = "Hello";
	char[] arrChar = strToCharArray.toCharArray();
	
	for(char i : arrChar) {
		System.out.println(i);
	}
	//Contains
	String strContains = "Hello Apa Kabar ?" ;
	if(strContains.contains("to"))
		System.out.println("Kalimat mengandung to");
	else
		System.out.println("Kalimat tidak mengandung to");
	
	//String convert to int
	String[] arrStr = {"1","2","3","4","5"};
	int[] arrInt = new int[arrStr.length];
	
	for(int i = 0; i < arrStr.length; i++) {
		arrInt[i] = Integer.valueOf(arrStr[i]);
	}
	System.out.println(Arrays.toString(arrInt));
	}

}
