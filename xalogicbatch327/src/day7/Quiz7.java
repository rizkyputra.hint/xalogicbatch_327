package day7;

import java.util.Scanner;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;

public class Quiz7 {

	public static void main(String[] args) throws ParseException {
		soal4();
	}

	static void soal1() {
		Scanner input = new Scanner(System.in);
		System.out.print("input : ");
		int count = input.nextInt();
		int n1 = 1, n2 = 1, n3, i;

		System.out.print(n1 + " " + n2);

		for (i = 2; i < count; ++i) {
			n3 = n1 + n2;
			System.out.print(" " + n3);
			n1 = n2;
			n2 = n3;
			input.close();
		}
	}

	static void soal2() {
		Scanner input = new Scanner(System.in);
		System.out.print("input : ");
		int text = input.nextInt();

		int[] bonarr = new int[text];
		for (int i = 0; i < bonarr.length; i++) {
			if (i < 3) {
				bonarr[i] = 1;
				System.out.print(bonarr[i] + " ");
			} else {
				bonarr[i] = bonarr[i - 1] + bonarr[i - 2] + bonarr[i - 3];

				if (i == bonarr.length - 1) {
					System.out.print(bonarr[i]);
				} else {
					System.out.print(bonarr[i] + " ");
				}
			}
		}
		System.out.println();
		System.out.println(Arrays.toString(bonarr).replace("]", "").replace("[", ""));
		input.close();
	}

	static void soal3() {

		Scanner input = new Scanner(System.in);
		int bilangan, prima;

		System.out.println("Prima");
		System.out.print("input : ");
		prima = input.nextInt();

		for (int i = 0; i < prima; i++) {
			bilangan = 0;
			for (int j = 1; j <= i; j++) {
				if (i % j == 0) {
					bilangan = bilangan + 1;
				}
			}
			if (bilangan == 2) {
				System.out.print(i + " ");
			}
			input.close();
		}
	}

//	static void soal4() throws ParseException {
//		
//		Scanner input = new Scanner(System.in);
//        System.out.print("input : ");
//        String jam = input.nextLine();
//		
//		SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss a");
//	       SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ss a");
//	       Date waktu = parseFormat.parse(jam + " PM");
//	       System.out.println("Output : " + displayFormat.format(waktu));
//	       input.close();
//	}
	static void soal4() {
		Scanner input = new Scanner(System.in);
		System.out.print("input: ");
		String jam = input.nextLine();

		String result = LocalTime.parse(jam, DateTimeFormatter.ofPattern("hh:mm:ssa"))
				.format(DateTimeFormatter.ofPattern("HH:mm:ss "));
		System.out.println(result);
		input.close();
	}

	static void soal5() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan sebuah bilangan: ");
		int x = input.nextInt();

		while (x != 1) {
			for (int i = 2; i <= x; i++) {
				if (x % i == 0) {
					System.out.print(x + "/" + i);
					x = x / i;
					System.out.println("=" + x);
					break;
				}
				input.close();
			}
		}

	}
}
