package day7;

import java.util.Arrays;

public class LatConvertArray {

	public static void main(String[] args) {
		
		KonversiArrayInttoString();
	}
	
	static void KonversiArrayStringToInt() {
		String[] array = {"1","2","3","4","5"};
		int[] arrInt = new int[array.length];
		for(int i=0; i<array.length; i++) {
			arrInt[i]= Integer.valueOf(array[i]);
			
		}
		int[] arrInt2 = Arrays.stream(array).mapToInt(Integer::valueOf).toArray();
		System.out.print(Arrays.toString(arrInt));
		System.out.print(Arrays.toString(arrInt2));
	}

	static void KonversiArrayInttoString() {
		int[] array = {1,2,3,4,5};
		String[] arrStr = new String[array.length];
		for(int i=0; i<array.length; i++) {
			arrStr[i]= String.valueOf(array[i]);
		}
		String[] arrStr2 = Arrays.stream(array).mapToObj(String::valueOf).toArray(String[]::new);
		System.out.print(Arrays.toString(arrStr));
		System.out.print(Arrays.toString(arrStr2));
	}
}