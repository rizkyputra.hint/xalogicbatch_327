package day7;

import java.util.Scanner;

public class LatRekursif {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		rekursif();
	}

	static void rekursif() {
		System.out.println("== Rekursif Function ==");
		System.out.print("Masukan input awal : ");
		int start = input.nextInt();
		System.out.print("input akhir : ");
		int end = input.nextInt();
		System.out.print("input tipe : ");
		String type = input.next();
		if (start <= end)
			Perulangan(start, end, type);
		else
			System.out.println("input awal gaboleh lebih besar dari input akhir");
	}

	static int Perulangan(int start, int end, String type) {
		if (type == "ASC") {
			if (start == end) {
				System.out.println(end);
				return end;
			}
			System.out.println(end);
			return Perulangan(start, end - 1, type);
		} else {
			if (start == end) {
				System.out.println(start);
				return start;
			}
			System.out.println(start);
			return Perulangan(start+1, end, type);
		}
	}
}
