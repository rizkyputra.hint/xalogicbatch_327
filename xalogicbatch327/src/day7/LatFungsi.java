package day7;
import java.util.Scanner;

public class LatFungsi {
	String nama = "";
	Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String nama2 = "";
		int nomor = 0;
		
		cetak2(); //manggil Fungsi static

		LatFungsi namaClass = new LatFungsi();		//Non Static
		namaClass.cetak();
		
		//manggil fungsi dengan parameter
		System.out.println("Luas persegi : " + luasPersegi(5));
		System.out.println("Luas persegi panjang : " + luasPersegiPanjang(5,10));
}
	void cetak() {
		System.out.println("Non static");
	}
	static void cetak2() {
		System.out.println("Cetak statik");
	}
	static int luasPersegi(int sisi) {
		int luas = sisi*sisi;
		return sisi * sisi;
	}
	//Fungsi dengan returnnya dan lebih dari 1 parameter
	static int luasPersegiPanjang(int panjang, int lebar) {
		int luas = panjang*lebar;
		return luas; 
	}
	//KONSEP OVERLOADING
	static int luasPersegi(double sisi) {
		return (int) (sisi*sisi);
	}
}