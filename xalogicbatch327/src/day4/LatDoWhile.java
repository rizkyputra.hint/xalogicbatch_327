package day4;

public class LatDoWhile {

	public static void main(String[] args) {
		int i = 1;
		
		do {
			System.out.println(i);
			i++;
		}
		while(i<=10);
	}

}
