package day8;

public class Overloading {

	public static void main(String[] args) {
	
		honk();
		honk("Mobil ford mustang");
	}
	public static void honk() {
		System.out.println("ini");
	}
	public static void honk(String car) {
		System.out.println(car + " agak lain");
	}
}
