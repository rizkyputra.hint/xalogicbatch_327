package day8;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
public class LatDateTime {

	public static void main(String[] args) {
	dateTime();

	}
	static void dateTime() {
		LocalDate date = LocalDate.now(); //Tahun Bulan Tanggal
		System.out.println(date);
		
		LocalTime time = LocalTime.now(); //Jam
		System.out.println(time);
		
		LocalDateTime ldt = LocalDateTime.now();
		System.out.println(ldt);
		
		DateTimeFormatter formatdate = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String strFormatDate = ldt.format(formatdate);
		System.out.println(strFormatDate);
		
		System.out.println(date.plusDays(5));
		System.out.println(date.plusDays(-5));
		
		LocalDate startdate = LocalDate.parse("2012-08-09");
		System.out.println(startdate);
		LocalDate enddate = LocalDate.parse("2012-09-10");
		System.out.println(enddate);
		
		long daycount = ChronoUnit.DAYS.between(startdate, enddate); //ngitung selisih hari
		System.out.println(daycount);
		long monthcount = ChronoUnit.MONTHS.between(startdate, enddate); //ngitung selisih hari
		System.out.println(monthcount);
		long yearscount = ChronoUnit.YEARS.between(startdate, enddate); //ngitung selisih hari
		System.out.println(yearscount);
	
		LocalDateTime startdate2 = LocalDateTime.parse("2019-08-20T10:01:00"); //NGITUNG SELISIH JAM
		System.out.println(startdate2);
		LocalDateTime enddate2 = LocalDateTime.parse("2019-08-21T10:01:00");
		
		long hourcount = ChronoUnit.HOURS.between(startdate2, enddate2);
		System.out.println(hourcount);
		
		long minutecount = ChronoUnit.MINUTES.between(startdate2, enddate2);
		System.out.println(minutecount);
		
		String time2 = "23:15";
		String result = LocalTime.parse(time2, DateTimeFormatter.ofPattern("HH:MM")).format(DateTimeFormatter.ofPattern("hh:mm a"));
		System.out.println(result);
	}

}
