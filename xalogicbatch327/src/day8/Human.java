package day8;

public class Human {
	
	//Field atau Properties
	public String name;
	public String gender;
	
	public static long count;
	
	public Human(String _name) {		//Constructor dengan parameter
		name = _name;
	}
	
	public Human() {					//Constructor Default
		
	}
	
	
	public static void main(String[] args) {
		
		//instannce class
		Human human;
		Human human2 = new Human("Agoda");
		
		System.out.println(human2.name);
		
		//isi field class
		human2.name="Peter Parker";
		
		//Simpan variable
		String dname = human2.name;
		
	 
	}

}
